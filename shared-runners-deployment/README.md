# Production Readiness Guide

For any new or existing large feature set the questions in this guide help to make them more robust and prevent impacting the availability of our platform.

Initially, this guide is likely to be used by Production Engineers who are embedded with other teams working on existing services and features. However, anyone working on a new feature set is encouraged to use this guide as well.

The goal of this guide is to help others understand how the new feature set may impact the rest of the (production) system; what steps need to be taken (besides deploying this new feature) to ensure that it can be properly managed; and to understand what it will take to manage the reliability of the new system / feature / service beyond its' initial deployment.

For readiness review of *infrastructure services* use this issue template instead:
[service_readiness.md](service_readiness.md)

## Summary

### **Provide a high level summary of this new product feature. Explain how this change will benefit GitLab customers. Enumerate the customer use-cases.**

This work is being tracked in the following epic https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/456

The blue/green deployment of the shared runners fleet is a big improvement in operations, which means we'll be able to respond faster to incidents, and deployments won't take as much hand-holding.

The current deployment strategy is going to each runner and running commands over SSH to drain the jobs, apply a change and restart the service. This is done sequentially on our 5+ machines and takes hours each, due to the long timeouts of CI jobs (each can run max 3 hours). So the engineer deploying a change has to spend a full day taking care of these pet instances.

In the new model, we reduce the individual interactions needed for the deployment. We have two runner deployments (blue & green) to follow [blue green deployment method](https://docs.aws.amazon.com/whitepapers/latest/overview-deployment-options/overview-deployment-options.pdf#bluegreen-deployments). Applying a change becomes starting up the inactive deployment (can be done very quickly), and draining the old one in the background. The draining task is accompplished by a CI job, triggered via chatops. So the engineer deploying the change doesn't have to manually hand-hold each instance. We're also able to parallelize the draining process.

### **What metrics, including business metrics, should be monitored to ensure will this feature launch will be a success?**

The business metric this will improve is deployment time will be reduced by 80%.

For technical metrics, the usual runner metrics will need to be followed as we deploy this feature:

- Apdex
- Error rate of failed build (can point an invalid configuration)
- Number of running jobs per runner manager
- Saturation metrics
- Quota usage

These can be found in the runner dashboards:

- https://dashboards.gitlab.net/d/ci-runners-deployment/ci-runners-deployment-overview
- https://dashboards.gitlab.net/d/ci-runners-incident-autoscaling/ci-runners-incident-support-autoscaling
- https://dashboards.gitlab.net/d/ci-runners-incident-database/ci-runners-incident-support-database
- https://dashboards.gitlab.net/d/ci-runners-incident-gitlab-application/ci-runners-incident-support-gitlab-application
- https://dashboards.gitlab.net/d/ci-runners-incident-runner-manager/ci-runners-incident-support-runner-manager

The quota usage is not in our dashboards, but can be viewed below. Look at `Heavy-weight read requests` and `Operation read requests`

- https://console.cloud.google.com/apis/api/compute.googleapis.com/quotas?project=gitlab-ci-plan-free-3-35411a
- https://console.cloud.google.com/apis/api/compute.googleapis.com/quotas?project=gitlab-ci-plan-free-4-3ba81e
- https://console.cloud.google.com/apis/api/compute.googleapis.com/quotas?project=gitlab-ci-plan-free-5-6ef84d
- https://console.cloud.google.com/apis/api/compute.googleapis.com/quotas?project=gitlab-ci-plan-free-6-f2de7a
- https://console.cloud.google.com/apis/api/compute.googleapis.com/quotas?project=gitlab-ci-plan-free-7-7fe256

## Architecture

### **Add architecture diagrams to this issue of feature components and how they interact with existing GitLab components. Include internal dependencies, ports, security policies, etc.**

```mermaid
graph TD
subgraph GitLab.com
  A[API End Point]
end
subgraph GCP GitLab CI
  subgraph CI Network
    subgraph AD [Active Deployment]
      G[Shared Green 1] --> |Public Internet| A
      H[Shared Green 2]
    end

    subgraph Z [Inactive Deployment]
      I[Shared Blue 1]
      J[Shared Blue 2]
    end
  end
  subgraph Runner GKE Network
    subgraph K [K8s Cluster]
      D[Deployer K8s Executor] -- Executes --> C;
      C[Chatops Job] --> |SSH Over Peering| G
      C[Chatops Job] --> |SSH Over Peering| I
      P[Prometheus]
    end
  end
end
```

### **Describe each component of the new feature and enumerate what it does to support customer use cases.**

  - Chatops: deployment jobs are triggered via chat commands in Slack: `/runner run COMMAND SHARD DEPLOYMENT`, for example: `/runner run stop private blue`. 
    - [Blue green deployment runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/ci-runners/linux/blue-green-deployment.md). 
    - To run the chatops command you need `Developer` access to the `gitlab-com/gl-infra/ci-runners/deployer` repository in `ops.gitlab.net`.
    - At this moment we support `start`, `stop` and `chef` commands. The former start and stop chef and the runner service, the latter only runs chef and assumes it is already active.
  - `ops.gitlab.net` CI pipeline: the deployment job is executed by our ops instance
  - `deployer` runner: the runner executing the job is separated from the fleet it deploys. It lives in the `runners-gke` cluster in the `gitlab-ci-155816` GCP project. It is deployed via `helm` from https://gitlab.com/gitlab-com/gl-infra/ci-runners/deployer by the runner team. This is needed because we can't run the job for a deployment on the instances we're actually deploying.
  - active deployment: these are the instances currently executing the job
  - inactive deployments: these are the instances not executing jobs. They have chef disabled and the runner service stopped, and are ready to start working

### **For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?**

  - chatops: we can still trigger CI jobs manually, so only a minor inconvenience (need to figure out the right pipeline and variables to trigger)
  - `ops.gitlab.net`: ops being unavailable means we can't deploy in an automated fashion and have to manually SSH into each instances and run commands. Significant inconvenience but the commands are simple (start chef, start the runner)
  - `deployer` runner: the dedicated runner being unavailable has the same impact as ops being unavailable

### **If applicable, explain how this new feature will scale and any potential single points of failure in the design.**

We don't foresee scaling to a number of instances or deployments that would raise scaling problems. Current instance count is < 10 and this number should double (active+inactive deployments).

## Operational Risk Assessment

### **What are the potential scalability or performance issues that may result with this change?**

If we deploy when we're at saturation, we risk running into GCP quotas. That's because for a short time we double our capacity during the deploy.

+Our capacity as a whole after this is released is not changing. We are going to have the same number of runner managers, with the same number for the `concurrent` setting so GCP quotas and are already in place of that load.

### **List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the it will be impacted by a failure of that dependency.**

Stricly speaking about the deployment, this has no dependencies besides ops and the dedicated runner. Of course the runners depend on Gitlab's API to be available and GCP API to be available (for ephemeral runners autoscaling purpose).

### **Were there any features cut or compromises made to make the feature launch?**

We are tracking those in the following epic https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/581

### **List the top operational risks when this feature goes live.**

  - New GCP instances: the runner workload will move from our older, long-lived runners to fresh instances. It could be that a unknown/legacy configuration change exists on the old instances that we don't know about
  - We have created new chef roles and associated configuration, as well as new runner tokens, and could have made an error there
  - When a blue/green switchover happens, for a short time during the deployment we double our capacity. If we run a deployment when we're at saturation, we risk running into GCP quotas

If any of these scenarios develop, the fastest way to stop the new instances is to run `/runner run stop shared blue` and `/runner run stop shared green`. This assumes the old runners are still available. We'll detail our rollback plan in the change management issue.

### **What are a few operational concerns that will not be present at launch, but may be a concern later?**

Once we decide to destroy the inactive instances, or move to more dynamic scaling up/down, maintaining the Cloudflare allowlist by static public IP will be an issue https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/14385

### **Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?**

We will keep around the old runner instances, in a deactivated state. It will be easy to start them up again (start chef, start runner service):

```
knife ssh -afqdn 'roles:gitlab-runner-srm' -- 'sudo -i chef-client-enable'
knife ssh -afqdn 'roles:gitlab-runner-srm' -- 'sudo -i chef-client'
knife ssh -afqdn 'roles:gitlab-runner-srm' -- 'sudo -i systemctl enable gitlab-runner'
```

### **Document every way the customer will interact with this new feature and how customers will be impacted by a failure of each interaction.**

No customer interaction

### **As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?**

Worst case scenario is the new instances failing to process jobs. We will mitigate that by doing a progressive rollout based on increments of production traffic, similarly to how we deployed blue/green for `private` and `gitalb-org-shared` shards

## Database

### **If we use a database, is the data structure verified and vetted by the database team?**

Not applicable

### **Do we have an approximate growth rate of the stored data (for capacity planning)?**

Not applicable

### **Can we age data and delete data of a certain age?**

Not applicable

## Security and Compliance

### **Were the [gitlab security development guidelines](https://about.gitlab.com/security/#gitlab-development-guidelines) followed for this feature?**

Yes

### **If this feature requires new infrastructure, will it be updated regularly with OS updates?**

Correct, the new instances use our regular chef setup, so they behave similarly to existing infrastructure.

### **Has effort been made to obscure or elide sensitive customer data in logging?**

This feature does not introduce new logging

### **Is any potentially sensitive user-provided data persisted? If so is this data encrypted at rest?**

This feature does not introduce new customer data handling

### **Is the service subject to any regulatory/compliance standards? If so, detail which and provide details on applicable controls, management processes, additional monitoring, and mitigating factors.**

This feature is not subject to any standards

## Performance

### **Explain what validation was done following GitLab's [performance guidlines](https://docs.gitlab.com/ce/development/performance.html) please explain or link to the results below**
    * [Query Performer](https://docs.gitlab.com/ce/development/query_recorder.html)
    * [Sherlock](https://docs.gitlab.com/ce/development/profiling.html#sherlock)
    * [Request Profiling](https://docs.gitlab.com/ce/administration/monitoring/performance/request_profiling.html)

None, this feature does not change the performance characteristic of the runners (identical instances and configuration)

### **Are there any potential performance impacts on the database when this feature is enabled at GitLab.com scale?**

None

### **Are there any throttling limits imposed by this feature? If so how are they managed?**

GCP API quotas; identical to current infrastructure

### **If there are throttling limits, what is the customer experience of hitting a limit?**

Builds are failing

### **For all dependencies external and internal to the application, are there retry and back-off strategies for them?**

We don't retry any job by design, because some jobs can't be retried and this decision is left to the user. So all errors are bubbled up to our users.

We do have retry and back-off strategies for docker-machine instance management.

We also have retries for saving build traces, job logs, artifacts, etc. - in the GitLab Runner -> GitLab API communication.

### **Does the feature account for brief spikes in traffic, at least 2x above the expected TPS?**

Yes, we overprovision our runner capacity, with the same capacity we have right now. You can take a look at the [saturation metrics](https://dashboards.gitlab.net/d/ci-runners-main/ci-runners-overview?viewPanel=1217942947&orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main)

## Backup and Restore

### **Outside of existing backups, are there any other customer data that needs to be backed up for this product feature?**

None

### **Are backups monitored?**

Not applicable

### **Was a restore from backup tested?**

Not applicable

## Monitoring and Alerts

### **Is the service logging in JSON format and are logs forwarded to logstash?**

Yes

### **Is the service reporting metrics to Prometheus?**

Yes

### **How is the end-to-end customer experience measured?**

The Runner Appdex measures if the "fair scheduling algorithm" is respected: jobs from projects, that have no more than 4 jobs already running on instance runners, need to start bellow 1 minute.

### **Do we have a target SLA in place for this service?**

Appdex score above 99.95%

### **Do we know what the indicators (SLI) are that map to the target SLA?**

SLIs on the shared runner's queue, runners failing to get jobs, job failures with `runner_system_failure` reason, trace archiving

They will be tracked the same as the old runners.

### **Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?**

Yes

### **Do we have troubleshooting runbooks linked to these alerts?**

Yes

### **What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?**

We don't have a guidance specific to runners

### **do the oncall rotations responsible for this service have access to this service?**

Yes

## Responsibility

### **Which individuals are the subject matter experts and know the most about this feature?**

  - @tmaczukin @akohlbecker are engineers from the runner team with @erushton as manager
  - @steveazz is now SRE but has been working on this feature in his runner days

### **Which team or set of individuals will take responsibility for the reliability of the feature once it is in production?**

The Runner Group

### **Is someone from the team who built the feature on call for the launch? If not, why not?**

The Runner engineers are not strictly on-call as the runner team does not have an on-call rotation. We will be doing the launch ourselves and be around afterwards. Runner Engineers are only available EMEA timezones and can pinged on Slack in either #g_runner or #f_linux_shared_runners

## Testing

### **Describe the load test plan used for this feature. What breaking points were validated?**

No load testing has been done since no performance impacts are expected

### **For the component failures that were theorized for this feature, were they tested? If so include the results of these failure tests.**

No test has been done as the failure modes from ops, the deployer runner, GCP and Slack are known.

### **Give a brief overview of what tests are run automatically in GitLab's CI/CD pipeline for this feature?**

None
