# Product Analytics

## Experiment

### Service Catalog

_The items below will be reviewed by the Reliability team._

- [x]  [x] Link to the [service catalog entry](https://gitlab.com/gitlab-com/runbooks/-/blob/17d9104ec600a8ea3a14db222de18da974697cd4/services/service-catalog.yml#L421-435) for the service. Ensure that the following items are present in the service catalog, or listed here:
  - Summary:

    The Product Analytics Stack (hereafter referred to as the Stack) is a group of applications intended for installation on Kubernetes, on various cloud platforms. The Stack is intended to be managed similar to how our CI Runners operate, in that users can leverage the GitLab managed instances of the Stack, or deploy their own, giving them full control of their analytics data.

    Currently, the Stack is designed for usage on GitLab.com, where we intend to provide a managed service, providing shared and private instances of the Stack to enable usage of Product Analytics features for SaaS customers. The Stack does contain in-cluster services like Clickhouse for the eventual support for self-managed installations for users who want complete control, or operate in air-gapped or other specialized environments.

    The Stack is intended to address the problems of personas using product analytics for web applications. We want to provide a built-in manner for which GitLab users can measure the impact their applications are having on users, as they use our platform to design, develop, and deploy their products.

    Per our [direction page](https://about.gitlab.com/direction/analytics/product-analytics/):

    > Our initial use case is focused on our ability to dogfood. As a result they focus on Web Apps. Our initial use case is:
    > - Web applications (specifically Ruby on Rails)
    > - With a particular interest in views, funnels, and conversion
    > - Those that are already instrumented with Snowplow events

    The Stack will serve as a foundation for allowing GitLab to enrich product analytics data with the intrinsic value we have on users using our platform. We can give them insight into the impact their applications have on users by also showing them when feature flags, deployments, and other changes throughout GitLab, as they are viewing traffic, funnels, and other event data being instrumented and sent through the Stack.
  - Link to the [Architecture Design Workflow](https://about.gitlab.com/handbook/engineering/architecture/workflow/) for this feature, if there wasn't a design completed for this feature please explain why.
    - https://gitlab.com/gitlab-org/gitlab/-/merge_requests/132153
  - List the feature group that created this feature/service and who are the current Engineering Managers, Product Managers and their Directors.
    - https://about.gitlab.com/handbook/engineering/development/analytics/product-analytics/
  - List individuals are the subject matter experts and know the most about this feature.
    - https://about.gitlab.com/handbook/engineering/development/analytics/product-analytics/#team-members
  - List the team or set of individuals will take responsibility for the reliability of the feature once it is in production.
    - @dennis
  - List the member(s) of the team who built the feature will be on call for the launch.
    - @dennis
    - @mwoolf
  - List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the service will be impacted by a failure of that dependency.
    - Snowplow
      - Collector: Failure results in complete loss of event ingestion, nullifying downstream components.
      - Enricher: Failure results in stalled processing of events, resulting in a backlog of raw events in Kafka
    - [Configurator](https://gitlab.com/gitlab-org/analytics-section/analytics-configurator): Failure results in the failure to onboard new projects to Product Analytics as well as any usage/billing reporting, event pipeline is otherwise unaffected.
    - Kafka: Failure results in event collection, processing, and delivery.
    - Vector: Failure results in no events delivered to Clickhouse, event pipeline will eventually have a backlog of processed events that need to be stored.
    - Clickhouse Cloud: Failure results in no events stored permanently, failed reporting functionality. Event pipeline will eventually get backed up with processed events needing to be stored.
    - Cube: Failure results in failure to render dashboards, or perform any querying on the collected data.

### Infrastructure

_The items below will be reviewed by the Reliability team._

- [x] Do we use IaC (e.g., Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?
  - Yes, example: https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/master/environments/pre/analytics.tf
- [x] Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)?
  - GCP load balancers
- [x] Are all cloud infrastructure resources labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines?

### Operational Risk

_The items below will be reviewed by the Reliability team._

- [x] List the top three operational risks when this feature goes live.
  - Firstly, given how the Stack is designed and integrated with GitLab, disruptions to GitLab.com should be minimal. The operational risks speak strictly for Product Analytics as a service offered to customers.

    1. Part of the Stack fails, and impacts the event pipeline.
          - This compromises the value to customers when we hit General Availability, but until then, users are made aware that no promises are made with regards to stability, reliability, or any general quality of service.
      2. RED data is mishandled throughout the Stack.
            - This has been addressed by encrypting all data at rest and in transit through the Stack.
            - We also intend to limit network access to external-facing services before allowing users to freely opt in.
    3. The Stack is subjected to abuse, or DDoS attacks, given the collector endpoints are public-facing, incurring additional costs to maintain the service.
        - We intend to implement rate limiting controls, once we learn how customers use our feature.
- [x] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?

  - Stacks being separate to the rest of the GitLab architecture already minimize the blast radius. Please refer to the beginning of the Architecture section for more details.

    Beyond that, we intend to offer the capability to [allow users to bring their own Stack](https://gitlab.com/groups/gitlab-org/-/epics/8921), as an alternative solution that would inherently reduce impact on scalability and performance, therefore isolating the blast-radius. This eventually opens up the possibility for private, but GitLab-managed Stacks, similar to a GitLab Dedicated offering.

    As previously mentioned, the Stack will eventually be offered as a self-managed solution, further minimizing the blast radius.

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [ ] Link to the [metrics catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/metrics-catalog/services) for the service

### Deployment

_The items below will be reviewed by the Delivery team._

- [x] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
  -  https://gitlab.com/gitlab-com/gl-infra/production/-/issues/14608
- [x] Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?
  - Yes, `product_analytics_admin_settings`, `combined_analytics_dashboards`, and `product_analytics_dashboards`.
    - `product_analytics_admin_settings` ([Feature flag rollout issue](https://gitlab.com/gitlab-org/gitlab/-/issues/385602)) controls the visibility of the instance- and project-level settings whereby a user can connect their GitLab instance to a Product Analytics cluster
    - `combined_analytics_dashboards` ([Feature flag rollout issue](https://gitlab.com/gitlab-org/gitlab/-/issues/389067)) controls the visibility of the project-level combined dashboard listing which is the home for all future dashboards, including Product Analytics and Value Stream Dashboards. Eventually, other existing dashboards could be migrated here.
    - `product_analytics_dashboards` ([Feature flag rollout issue](https://gitlab.com/gitlab-org/gitlab/-/issues/398653)) controls the visibility of actual dashboards related to Product Analytics, within the combined analytics dashboards listing.

    These features ultimately control the visibility and usage of Product Analytics features so they would at most inconvenience users, but they wouldn't have any impact on the stability or availability of GitLab.com.

    Disabling the feature flags would reduce any negative impacts in the case there were bugs or unexpected load, but that would be specific to Product Analytics since the GitLab monolith effectively proxies all requests to the separate service.

- [x] How are the artifacts being built for this feature (e.g., using the [CNG](https://gitlab.com/gitlab-org/build/CNG/) or another image building pipeline).
  - We build a Helm chart and publish it to the Package Registry in ops.gitlab.net: https://gitlab.com/gitlab-org/analytics-section/product-analytics/analytics-stack/-/blob/main/.gitlab-ci.yml?ref_type=heads#L73-103
    - The source code for the Helm chart is available here: https://gitlab.com/gitlab-org/analytics-section/product-analytics/analytics-stack
  - We created a container called `[analytics-configurator](https://gitlab.com/gitlab-org/analytics-section/analytics-configurator)` which has container scanning enabled when a new release is tagged. This process is automated by CI pipeline.
    - Its upstream container is `golang:alpine`.

### Security Considerations

_The items below will be reviewed by the Infrasec team._

- [x] Link or list information for new resources of the following type:
  - AWS Accounts/GCP Projects: N/A
  - New Subnets: N/A
  - VPC/Network Peering: N/A
  - DNS names: N/A
  - Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...):
    - https://collector.product-analytics.env.gke.gitlab.net
    - https://cube.product-analytics.env.gke.gitlab.net
    - https://configurator.product-analytics.env.gke.gitlab.net
  - Other (anything relevant that might be worth mention):
- [x] Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?
- [x] Was an [Application Security Review](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/appsec-reviews/) requested, if appropriate? Link it here.
  - [Product Analytics Stack AppSec Review](https://gitlab.com/gitlab-com/gl-security/appsec/appsec-reviews/-/issues/215)
- [ ] Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...). For example, using unattended upgrade or [renovate bot](https://github.com/renovatebot/renovate) to keep dependencies up-to-date?
- [x] For IaC (e.g., Terraform), is there any secure static code analysis tools like ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov))? If not and new IaC is being introduced, please explain why.
- [x] If we're creating new containers (e.g., a Dockerfile with an image build pipeline), are we using `kics` or `checkov` to scan Dockerfiles or [GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities?
  - Yes, https://gitlab.com/gitlab-org/analytics-section/product-analytics/analytics-stack/-/blob/main/.gitlab-ci.yml?ref_type=heads#L2-5

### Identity and Access Management

_The items below will be reviewed by the Infrasec team._

- [x] Are we adding any new forms of Authentication (New service-accounts, users/password for storage, OIDC, etc...)?
  - Yes, for inter-service communications, but nothing that is customer-facing.
  - **All credentials are handled as secrets managed by Hashicorp Vault**
  - The Configurator has user/password auth for Clickhouse database management
  - Clickhouse Clickhouse has auth for performing databases operations
  - Cube has security contexts to only allow namespaces to see their own data.
- [x] Was effort put in to ensure that the new service follows the [least privilege principle](https://en.wikipedia.org/wiki/Principle_of_least_privilege), so that permissions are reduced as much as possible?
- [ ] Do firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)?
- [ ] Is the service covered by a [WAF (Web Application Firewall)](https://cheatsheetseries.owasp.org/cheatsheets/Secure_Cloud_Architecture_Cheat_Sheet.html#web-application-firewall) in [Cloudflare](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/cloudflare#how-we-use-page-rules-and-waf-rules-to-counter-abuse-and-attacks)?

### Logging, Audit and Data Access

_The items below will be reviewed by the Infrasec team._

- [x] Did we make an effort to redact customer data from logs?
- [x] What kind of data is stored on each system (secrets, customer data, audit, etc...)?
  - Customer data is handled in transit, but not stored in this architecture. Customer data is stored in ClickHouse Cloud, a third-party solution approved for RED data.
  - The Configurator stores namespace IDs and their corresponding tracking IDs used to route event data to the right database.
- [x] How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is RED)?
  - RED
- [~] Do we have audit logs for when data is accessed? If you are unsure or if using Reliability's central logging and a new pubsub topic was created, create an issue in the [Security Logging Project](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/security-logging/security-logging/-/issues/new?issuable_template=add-remove-change-log-source) using the `add-remove-change-log-source` template.
  - No, we could emit audit events whenever people query data using our dashboard or API functionality, but otherwise you would have to defer to ClickHouse Cloud.
 - [ ] Ensure appropriate logs are being kept for compliance and requirements for retention are met.
 - [~] If the data classification = Red for the new environment, please create a [Security Compliance Intake issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/security-compliance-commercial-and-dedicated/security-compliance-intake/-/issues/new?issue[title]=System%20Intake:%20%5BSystem%20Name%20FY2%23%20Q%23%5D&issuable_template=intakeform). Note this is not necessary if the service is deployed in existing Production infrastructure.
    - Data is stored in ClickHouse Cloud, this service is in the process of being deployed in existing Production infrastructure: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/23773

## Beta

### Monitoring and Alerting

_The items below will be reviewed by the Reliability team._

- [x] Link to examples of logs on https://logs.gitlab.net
  - Logs are stored in the dashboard below as we use Loki/Grafana for logs rather than elastic.
  - https://dashboards.gitlab.net/d/da6cf9ea-d593-41ed-91c5-8536fd15c2fa/3c4746b9-23ce-5002-a9b2-59933336fbcf?orgId=1&refresh=5m
- [x] Link to the [Grafana dashboard](https://dashboards.gitlab.net) for this service.
  - https://dashboards.gitlab.net/d/da6cf9ea-d593-41ed-91c5-8536fd15c2fa/3c4746b9-23ce-5002-a9b2-59933336fbcf?orgId=1&refresh=5m

### Backup, Restore, DR and Retention

_The items below will be reviewed by the Reliability team._

- [ ] Are there custom backup/restore requirements?
  - We are currently in negotiations with ClickHouse Cloud to increase backup retention period which is currently 48 hours.
- [ ] Are backups monitored?
  - Backups are handled and monitored by ClickHouse Cloud.
- [ ] Was a restore from backup tested?
  - Yes. We are able to restore from backups using ClickHouse Cloud.
  - This process takes around 10 minutes.
  - Runbook is https://gitlab.com/gitlab-org/analytics-section/runbooks/-/blob/main/clickhouse-backup-restore.md
- [ ] Link to information about growth rate of stored data.
  - We have two databases, one specifically for data captured from GitLab.com. Currently this is growing at a rate of ~100GB per month. This growth rate is likely to decrease as we have recently opted to store less data.
  - The other database will store data from customer projects. This is currently growing at a rate of around 13GB per month. This will likely increase once we release to Beta.
  - Data is stored by ClickHouse Cloud in AWS S3 so we do expect capacity issues.

### Deployment

_The items below will be reviewed by the Delivery team._

- [ ] Will a [change management issue](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/) be used for rollout? If so, link to it here.
  - No. Not required according to handbook criteria.
- [ ] Does this feature have any version compatibility requirements with other components (e.g., Gitaly, Sidekiq, Rails) that will require a specific order of deployments?
  - No.
- [ ] Is this feature validated by our [QA blackbox tests](https://gitlab.com/gitlab-org/gitlab-qa)?
  - Partially. Onboarding (which is the part most likely to fail) is covered by blackbox tests added in (this MR)[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/133605].
- [ ] Will it be possible to roll back this feature? If so explain how it will be possible.
  - Yes. All features can be disabled using the `product_analytics_dashboards` feature flag. That will effectively sever all links between GitLab and the analytics stack. It works globally, or actor-based.

Additional questions during review:

- Where is the configuration for the Stack located?
  - The stack is stored, maintained and deployed from https://gitlab.com/gitlab-org/analytics-section/product-analytics/analytics-stack
- How are components or the Helm chart updated?
  - All of the stack's components are updated in the repository above and the helm chart is updated using CI pipeline environments.
  - Secrets for the deployments are stored in Vault.

### Security

_The items below will be reviewed by the InfraSec team._

- [ ] Put yourself in an attacker's shoes and list some examples of "What could possibly go wrong?". Are you OK going into Beta knowing that?
  - Our biggest potential concern is data exfiltration. Data can be exposed using the `POST /api/v4/projects/:id/product_analytics/request/load` endpoint.
    - It is well-tested and requires authorization to use. 
    - In some cases (when `include_token = true`), that endpoint will generate a short-lived (1 day) project access token.
  - DDoS vectors potentially exist. When querying usage data via the GitLab API, it does make a call to `analytics-configurator` and by extension the underlying ClickHouse database.
    - (An issue)[https://gitlab.com/gitlab-org/gitlab/-/issues/430865] exists to address this, however the risk is very low for the vast majority of use-cases and the feature flag can be disabled for any troublesome projects if neccessary.
- [ ] Link to any outstanding security-related epics & issues for this feature. Are you OK going into Beta with those still on the TODO list?
  - There are no outstanding security issues or epics at this time.
